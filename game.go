package main

import (
	"image"
	"math"
	"os"
	"time"

	_ "image/png"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"golang.org/x/image/colornames"
)

func run() {
	cfg := pixelgl.WindowConfig{
		Title:  "Knox2D",
		Bounds: pixel.R(0, 0, 1024, 768),
		VSync:  true,
	}
	win, err := pixelgl.NewWindow(cfg)
	if err != nil {
		panic(err)
	}

	treeImage, err := loadPicture("tree.png")
	if err != nil {
		panic(err)
	}

	mapImage, err := loadPicture("map.png")
	if err != nil {
		panic(err)
	}

	var mapSprite = pixel.NewSprite(mapImage, mapImage.Bounds())
	var trees []*pixel.Sprite
	var coords []pixel.Vec

	var camPos = pixel.ZV
	var camSpeed = 500.0

	var mapRotation = 0.0
	var mapRotationSpeed = 1.0
	var mapPosition = win.Bounds().Center()

	camPos.X = mapPosition.X
	camPos.Y = mapPosition.Y

	// Main game loop
	last := time.Now()
	for !win.Closed() {
		cam := pixel.IM.Moved(win.Bounds().Center().Sub(camPos))
		win.SetMatrix(cam)

		dt := time.Since(last).Seconds()
		last = time.Now()
		win.Clear(colornames.Forestgreen)

		if win.Pressed(pixelgl.KeyQ) {
			mapRotation += mapRotationSpeed * dt
		}

		if win.Pressed(pixelgl.KeyE) {
			mapRotation -= mapRotationSpeed * dt
		}

		if win.Pressed(pixelgl.KeyA) {
			camPos.X -= camSpeed * dt
		}
		if win.Pressed(pixelgl.KeyD) {
			camPos.X += camSpeed * dt
		}
		if win.Pressed(pixelgl.KeyS) {
			camPos.Y -= camSpeed * dt
		}
		if win.Pressed(pixelgl.KeyW) {
			camPos.Y += camSpeed * dt
		}

		if win.JustPressed(pixelgl.MouseButtonLeft) {
			tree := pixel.NewSprite(treeImage, treeImage.Bounds())
			trees = append(trees, tree)

			mouse := win.MousePosition()
			position := TransformMouseCoords(mouse, mapPosition, mapRotation, camPos)

			coords = append(coords, position)
		}

		mat := pixel.IM
		mat = mat.Moved(mapPosition)
		mat = mat.Rotated(mapPosition, mapRotation)
		mapSprite.Draw(win, mat)

		for i, tree := range trees {
			var position = coords[i]

			tree.Draw(win, pixel.IM.Moved(LockPositionWithMapRotation(position, mapPosition, mapRotation)))
		}

		win.Update()
	}
}

func TransformMouseCoords(initialPosition pixel.Vec, mapPosition pixel.Vec, rotation float64, camPos pixel.Vec) pixel.Vec {

	pointCameraTransformed := initialPosition.Add(camPos.Sub(mapPosition))

	x := pointCameraTransformed.X - mapPosition.X
	y := pointCameraTransformed.Y - mapPosition.Y
	initialRotation := math.Atan2(y, x)
	distance := math.Sqrt(x*x + y*y)

	pX := mapPosition.X + math.Cos(initialRotation-rotation)*distance
	pY := mapPosition.Y + math.Sin(initialRotation-rotation)*distance
	position := pixel.V(pX, pY)

	return position
}

func LockPositionWithMapRotation(spritePosition pixel.Vec, mapPosition pixel.Vec, rotation float64) pixel.Vec {

	xDist := spritePosition.X - mapPosition.X
	yDist := spritePosition.Y - mapPosition.Y
	dist := math.Sqrt(xDist*xDist + yDist*yDist)

	initialRotation := math.Atan2(xDist, yDist)

	rX := math.Cos(-rotation + initialRotation)
	rY := math.Sin(-rotation + initialRotation)
	rV := pixel.V(rX, rY)

	if rV.X != 0 || rV.Y != 0 {
		rV = rV.Normal()
	}

	pX := mapPosition.X + (-rV.X * dist)
	pY := mapPosition.Y + (rV.Y * dist)
	pV := pixel.V(pX, pY)
	return pV
}

func loadPicture(path string) (pixel.Picture, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	img, _, err := image.Decode(file)
	if err != nil {
		return nil, err
	}
	return pixel.PictureDataFromImage(img), nil
}

func main() {
	pixelgl.Run(run)
}
